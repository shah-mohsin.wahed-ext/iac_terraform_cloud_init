#TP IAC v0.1
# author : Shah Mohsin WAHED
# date 10/18/2024

terraform {
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
    }
  }
}

provider "libvirt" {
  # Configuration du fournisseur libvirt
  uri = "qemu:///system"
}


# on utilise la derniere image ubuntu 22.10 dispo 
# https://cloud-images.ubuntu.com/oracular/current/oracular-server-cloudimg-amd64.img
resource "libvirt_volume" "os_image" {
  name = "${var.hostname}-os_image"
  pool = "default"
  source = "file:///home/cfssi/oracular-server-cloudimg-amd64.img"
  format = "qcow2"
}

# on genere une iso cloud-init
resource "libvirt_cloudinit_disk" "commoninit" {
  name = "${var.hostname}-commoninit.iso"
  pool = "default"
  user_data      = data.template_cloudinit_config.config.rendered
  network_config = data.template_file.network_config.rendered
}


# on gzip le cloud init userdata pour qu'il prenne moins de place
data "template_cloudinit_config" "config" {
  gzip = false
  base64_encode = false
  part {
    content     = templatefile("cloudInitLib/userdata.split/52.NGINX_solo.tftpl",{})
    content_type = "text/x-shellscript"
  }
}

# cloud init metadata
data "template_file" "network_config" {
  template = templatefile("cloudInitLib/metadata/metadata1nicDyn.tftpl",
                                    {
                                      hostname="${var.hostname}",
                                      ipA="dhcp",gw="none",dns="none"
                                    })
}


// Create the machine
resource "libvirt_domain" "domain-ubuntu" {
  # domain name in libvirt, not hostname
  name = "${var.hostname}"
  memory = var.memoryMB
  vcpu = var.cpu

  disk {
    volume_id = libvirt_volume.os_image.id
  }
  network_interface {
    network_name = "${var.network01}"
  }

  cloudinit = libvirt_cloudinit_disk.commoninit.id

  # IMPORTANT
  # Ubuntu can hang is a isa-serial is not present at boot time.
  # If you find your CPU 100% and never is available this is why
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = "true"
  }
}

output "ips" {
  value = libvirt_domain.domain-ubuntu.*.network_interface.0.addresses
}

