# ---------------------------------------------------------------------------------------------------------------------
# fichier de variable
# ---------------------------------------------------------------------------------------------------------------------

# variables that can be overriden
variable hostname {
  type = string
  description = "hostname"
  default = "test"
}

variable domain {
  type = string
  default = "example.com"
}

variable "network01" {
  type = string
}

variable ip_type {
  type = string
  default = "dhcp"
}

variable memoryMB {
  type = string
  default = "1024*1"
}

variable cpu {
  type = number
  default = 1
}
